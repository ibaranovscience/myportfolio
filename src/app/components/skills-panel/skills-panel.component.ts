import { Component, HostBinding, Input, Inject, HostListener } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-skills-panel',
  templateUrl: './skills-panel.component.html',
  styleUrls: ['./skills-panel.component.scss'],
  animations:[ 
    trigger('degree',
    [ 
      state('0', style({ transform: 'rotate(0)'})),
      state('-30', style({ transform: 'rotate(-30deg)'})),
      state('-60', style({ transform: 'rotate(-60deg)'})),
      state('-90', style({ transform: 'rotate(-90deg)'})),
      state('-120', style({ transform: 'rotate(-120deg)'})),
      state('-150', style({ transform: 'rotate(-150deg)'})),
      state('-180', style({ transform: 'rotate(-180deg)'})),
      state('-210', style({ transform: 'rotate(-210deg)'})),
      state('-240', style({ transform: 'rotate(-240deg)'})),
      state('-270', style({ transform: 'rotate(-270deg)'})),
      state('-300', style({ transform: 'rotate(-300deg)'})),
      state('-330', style({ transform: 'rotate(-330deg)'})),
      state('-360', style({ transform: 'rotate(-360deg)'})),
      transition('* => *', [
        animate(500)
      ]),
      // transition('0 => -30',[ animate(300)]),
      // transition('-30 => -60',[ animate(300)]),
      // transition('-60 => -90',[ animate(300)]),
      // transition('-90 => -120',[ animate(300)]),
      // transition('-120 => -150',[ animate(300)]),
      // transition('-150 => -180',[ animate(300)]),
      // transition('-180 => -150',[ animate(300)]),
      // transition('-210 => -180',[ animate(300)]),
      // transition('-240 => -210',[ animate(300)]),
      // transition('-270 => -240',[ animate(300)]),
      // transition('-300 => -270',[ animate(300)]),
      // transition('-330 => -300',[ animate(300)]),
      // transition('-360 => -330',[ animate(300)]),
      // transition('-360 => 0',[ animate(300)]),
    ]
  )]
})
export class SkillsPanelComponent {
  //@HostBinding('@fade')

  frontEndSkillsArray = [
    {
      title: 'React',
      icon: 'assets/svg/React-icon.svg'
    },
    {
      title: 'Redux',
      icon: 'assets/svg/icons8-redux.svg'
    },
    {
      title: 'MobX',
      icon: 'assets/svg/mobx.svg'
    },
    {
      title: 'Angular',
      icon: 'assets/svg/icons8-angular.svg'
    },
    
    {
      title: 'HTML',
      icon: 'assets/svg/icons8-html.svg'
    },
    {
      title: 'CSS',
      icon: 'assets/svg/icons8-css.svg'
    }
  ];

  systemSkillsArray = [
    {
      title: 'C#',
      icon: 'assets/svg/icons8-c-sharp-logo.svg'
    },
    {
      title: 'C++',
      icon: 'assets/svg/icons8-c.svg'
    },
    {
      title: 'Java',
      icon: 'assets/svg/icons8-java.svg'
    },
    {
      title: 'Python',
      icon: 'assets/svg/icons8-python.svg'
    },
    {
      title: 'PostgreSQL',
      icon: 'assets/svg/icons8-postgres.svg'
    },
    {
      title: 'MSSQL',
      icon: 'assets/svg/icons8-sql-server.svg'
    },
    {
      title: 'MongoDB',
      icon: 'assets/svg/mongodb-icon.svg'
    },
  ];

  technologySkillsArray = [
    {
      title: 'Android',
      icon: 'assets/svg/icons8-android.svg'
    },
    {
      title: 'React Native',
      icon: 'assets/svg/icons8-react-native.svg'
    },
    {
      title: '.NET',
      icon: 'assets/svg/icons8-.net-framework.svg'
    },
    {
      title: 'Spring',
      icon: 'assets/svg/icons8-spring-boot.svg'
    },
    {
      title: 'Docker',
      icon: 'assets/svg/icons8-docker.svg'
    },
    {
      title: 'Flask',
      icon: 'assets/svg/icons8-flask.svg'
    },
  ];

  allSkillsArray = [
    [...this.frontEndSkillsArray, ...this.frontEndSkillsArray],
    [...this.systemSkillsArray, ...this.systemSkillsArray],
    [...this.technologySkillsArray, ...this.technologySkillsArray],
  ]

  @Input() scrollY: number;

  getGridTemplate(length: number) {
    let result = "";
    for (let i=0; i<length; i++) {
      result += ' 25%';
    }
    return result;
  }

  getChunks(array: Array<any>, chunkSize: number) {
    let result = [];
    for (let i = 0; i < array.length; i += chunkSize) {
        const chunk = array.slice(i, i + chunkSize);
        result.push(chunk);
    }
    return result;
  }

  getMovingClass(index: number) {
    if (index % 2 === 0) {
      return "icon-moving-left";
    } else {
      return "icon-moving-right";
    }
  }

  getDegree() {
    switch (true) {
      case this.scrollY < 360:
        return 0;
      case this.scrollY < 480 &&  this.scrollY > 360:
        return -30;
      case this.scrollY < 560 &&  this.scrollY > 480:
        return -60;
      case this.scrollY < 660 &&  this.scrollY > 560:
        return -90;
      case this.scrollY < 720 &&  this.scrollY > 660:
        return -120;
      case this.scrollY < 800 &&  this.scrollY > 720:
        return -150;
      case this.scrollY < 880 &&  this.scrollY > 800:
        return -180;
      case this.scrollY < 960 &&  this.scrollY > 880:
        return -210;
      case this.scrollY < 1020 &&  this.scrollY > 960:
        return -240;
      case this.scrollY < 1080 &&  this.scrollY > 1020:
        return -270;
      case this.scrollY < 1140 &&  this.scrollY > 1080:
        return -300;
      case this.scrollY < 1200 &&  this.scrollY > 1140:
        return -330;
      case this.scrollY < 1260 &&  this.scrollY > 1200:
        return -360;
      default:
        return 0;
    }
  }
}
