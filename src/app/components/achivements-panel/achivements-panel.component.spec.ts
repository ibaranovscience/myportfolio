import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AchivementsPanelComponent } from './achivements-panel.component';

describe('AchivementsPanelComponent', () => {
  let component: AchivementsPanelComponent;
  let fixture: ComponentFixture<AchivementsPanelComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AchivementsPanelComponent]
    });
    fixture = TestBed.createComponent(AchivementsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
