import { Component, HostBinding, Input, Inject, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations:[ 
    trigger('fade',
    [ 
      state('false', style({ opacity : 0})),
      state('true', style({ opacity : 1})),
      transition('true => false',[ animate(300)]),
      transition('false => true',[ animate(500)]),
    ]
  )]
})
export class HeaderComponent {
  @HostBinding('@fade')
  @Input() isHeaderVisible: boolean;
  selectedLanguage: string;
  public window: Window;
  faBars = faBars;
  menuChecked: Boolean;

  constructor(
    public translate: TranslateService,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.menuChecked = false;
    translate.addLangs(['en', 'ru']);
    translate.setDefaultLang('ru');
    this.selectedLanguage = 'ru';
    this.window = this.document.defaultView!;
  }
  
  switchLang(lang: string) {
    this.translate.use(lang);
    this.selectedLanguage = lang;
  }

  defaultTouch = { x: 0, y: 0, time: 0 };

  @HostListener('touchstart', ['$event'])
  @HostListener('touchend', ['$event'])
  @HostListener('touchcancel', ['$event'])
  handleTouch(event : any) {
      let touch = event.touches[0] || event.changedTouches[0];

      // check the events
      if (event.type === 'touchstart') {
          this.defaultTouch.x = touch.pageX;
          this.defaultTouch.y = touch.pageY;
          this.defaultTouch.time = event.timeStamp;
      } else if (event.type === 'touchend') {
          let deltaX = touch.pageX - this.defaultTouch.x;
          let deltaY = touch.pageY - this.defaultTouch.y;
          let deltaTime = event.timeStamp - this.defaultTouch.time;

          // simulte a swipe -> less than 500 ms and more than 60 px
          if (deltaTime < 500) {
              // touch movement lasted less than 500 ms
              if (Math.abs(deltaX) > 60) {
                  // delta x is at least 60 pixels
                  if (deltaX > 0) {
                      //this.doSwipeRight(event);
                  } else {
                      this.doSwipeLeft(event);
                  }
              }

              if (Math.abs(deltaY) > 60) {
                  // delta y is at least 60 pixels
                  if (deltaY > 0) {
                      this.doSwipeDown(event);
                  } else {
                      this.doSwipeUp(event);
                  }
              }
          }
      }
  }

  doSwipeLeft(event: any) {
    console.log('swipe left', event);
    this.menuChecked = false;
  }

  doSwipeRight(event: any) {
    console.log('swipe right', event);
    this.menuChecked = true;
  }

  doSwipeUp(event: any) {
      console.log('swipe up', event);
  }

  doSwipeDown(event: any) {
      console.log('swipe down', event);
  }
}
