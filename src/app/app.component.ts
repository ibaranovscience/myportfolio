import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Portfolio';
  constructor(@Inject(DOCUMENT) document : any) { }

  ngOnInit() {  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e : any) {  

    if (window.scrollY == 0) {
      return;
    }

     if (window.scrollY < this.prevScrollY) {
        if (Math.abs(window.scrollY - this.prevScrollY) > 50) {
          let element = document.getElementById('navbar');
          if (element) {
            this.isHeaderVisible = true;
          }
          this.prevScrollY = window.scrollY;
        }
     } else {
        let element = document.getElementById('navbar');
        if (element) {
          this.isHeaderVisible = false;
        }
        this.prevScrollY = window.scrollY;
     }
     console.log(this.prevScrollY);
  }

  isHeaderVisible = true;
  prevScrollY = 0;

}
